require 'digest/md5'

# Crack the code!
class Day5
  # Let be able to get the results and number of interations (for funsies).
  attr_accessor :result, :iterations

  # Set the layout we are using
  def initialize data, encrypted
    # Create results array
    @result = Array.new 0
    @iterations = 0
    # Loop until we have a full password.
    until @result.select { |v| !v.nil? }.size >= 8
      hash = Digest::MD5.hexdigest(data + @iterations.to_s)
      if hash.start_with?('00000')
        if encrypted
          # Check if the character is an integer.
          place = Integer(hash[5]) rescue nil
          # Add it to the array if it doesn't exist.
          @result[place] = hash[6] if place && place < 8 && place >= 0 && @result[place].nil?
        else
          @result << hash[5]
        end
      end
      @iterations += 1
    end
    @result = @result.join('')
  end

end
