require_relative '../helpers/point'

# Traverse a grid, if we are going to go off the grid don't move.
class Day2
  # Let be able to get the results.
  attr_accessor :result

  # Define layouts.
  @@layouts = {
    'square' => {
      Point.new(0, 0) => 1,
      Point.new(1, 0) => 2,
      Point.new(2, 0) => 3,
      Point.new(0, 1) => 4,
      Point.new(1, 1) => 5,
      Point.new(2, 1) => 6,
      Point.new(0, 2) => 7,
      Point.new(1, 2) => 8,
      Point.new(2, 2) => 9,
    },
    'diamond' => {
      Point.new(2, 0) => 1,
      Point.new(1, 1) => 2,
      Point.new(2, 1) => 3,
      Point.new(3, 1) => 4,
      Point.new(0, 2) => 5,
      Point.new(1, 2) => 6,
      Point.new(2, 2) => 7,
      Point.new(3, 2) => 8,
      Point.new(4, 2) => 9,
      Point.new(1, 3) => 'A',
      Point.new(2, 3) => 'B',
      Point.new(3, 3) => 'C',
      Point.new(2, 4) => 'D',
    }
  }

  # Convert the movements to cordinates
  @@actions = {
    'U' => Point.new(0, -1),
    'D' => Point.new(0, 1),
    'L' => Point.new(-1, 0),
    'R' => Point.new(1, 0),
  }

  # Set the layout we are using
  def initialize layout='square'
    @layout = @@layouts[layout]
  end

  # Take the code to traverse and the starting position and return the code.
  def input code, start_position='5'
    @key_combo = code.split("\n")
    # Get the cordinates of the starting position.
    @position = @layout.key(start_position)
    # Setup an array to push the results to.
    @result = []
    # Foreach line figure out the code for it.
    @key_combo.each do |key_combo|
      @result << @layout[key(key_combo.strip)]
    end
    @result.join('')
  end

  # Step through each character of the line for step data.
  def key key_combo
    key_combo.each_char do |step|
      move step
    end
    @position
  end

  # Move to the new position.
  def move step
    # Check if the cordinate is on the grid.
    new_position = @position + @@actions[step]
    @position = new_position unless @layout[new_position].nil?
  end

end
