require_relative '../helpers/point'

# Follow a set of instructions to figure out the path, the destination, and
# any time we've intersected a point we've already been on.

# A class to keep track of all path data.
class Day1
  # Let log and intersections be accessible.
  attr_accessor :log, :intersections

  # Create Point instances for each direction.
  @@directions = [
    Point.new(1, 0),  # North
    Point.new(0, 1),  # East
    Point.new(-1, 0), # South
    Point.new(0, -1)  # West
  ]

  # Setup a log to track every point we step through.
  # Track all intersections.
  # Set the default direction.
  # Run through the path.
  def initialize path
    @log = [Point.new(0, 0)]
    @intersections = []
    @dir_index = 0
    path.split(',').each do |cmd|
      move(cmd.strip)
    end
  end

  private
  # Rotate then move the appropriate amount of spaces.
  def move cmd
    # Loop through the directions array to get the correct face.
    @dir_index = (@dir_index + rotate(cmd.slice!(0))) % @@directions.size

    # Log each point we move through.
    (1..cmd.to_i).each do |n|
      # Generate the new step, which is just 1 forward in the current direction.
      step = @log.last.add @@directions[@dir_index]

      # Check if we've already been at this cordinate.
      if @log.include? step
        @intersections << step
      end

      # Add the step to the log.
      @log << step
    end
  end

  # If we rotate left we're moving back in the array, right is forward.
  def rotate cmd
    if cmd == 'L'
      -1
    else
      1
    end
  end
end
