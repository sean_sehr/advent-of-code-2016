class Triangle
  def initialize(x, y, z)
    @x = x
    @y = y
    @z = z
  end

  def isValid?
    points = [@x, @y, @z].sort
    points[0] + points[1] > points[2]
  end
end

# Check a list of data for valid triangles
class Day3
  # Let be able to get the results.
  attr_accessor :results

  # Set the layout we are using
  def initialize data, formt
    data_array = data.split("\n").map { |e| e.split.map(&:to_i) }
    # If in column for convert it.
    if formt == 'column'
      # Take the current arrays and step through them 3 at a time and flatten
      # the results.
      data_array = data_array.each_slice(3).flat_map do |chunk|
        # Zip them together
        chunk[0].zip(chunk[1], chunk[2])
      end
    end
    @results = data_array.map { |cords| Triangle.new(*cords) }
  end

  # Get the number of valid triangles
  def valid_triangles
    @results.select { |triangle| triangle.isValid? }.size
  end
end
