# Create a room to check if it's real.
class Room
  attr_accessor :decoded_name, :value
  def initialize name
    @value = name[/[\d]+/].to_i
    # Scan returns an array of arrays.
    @key = name.scan(/\[(.*?)\]/).first.first
    @code = name.split('[').first
    name = @code.split('-')
    name.pop
    @decoded_name = name.map do |word|
      word.each_char.map { |c| (((c.ord - 'a'.ord + @value) % 26) + 'a'.ord).chr }.join
    end.join(' ')
  end

  # Check the name to see if it's a valid room.
  def isValid?
    # Create a hash to count the number of instances for each character.
    chrs_count = Hash.new 0
    # Only grab alphabetic characters, scan gives an array of arrays.
    chrs = @code.scan(/([[:alpha:]])/).flatten

    chrs.each { |chr| chrs_count[chr] += 1  }

    # Sort by the count or fall back to alphabetical sorting.
    chrs.uniq.sort_by{ |v| [-chrs_count[v], v] }.join.start_with? @key
  end
end

# Check a list of data for valid rooms and get the checksum of the room numbers.
class Day4
  # Let be able to get the rooms.
  attr_accessor :rooms

  # Create the rooms.
  def initialize data
    room_names = data.split("\n").map(&:strip)
    @rooms = room_names.map do |room_name|
      Room.new(room_name)
    end
  end

  # Get the checksum of all the valid rooms.
  def checksum
    @rooms.select { |room| room.isValid? }.inject(0) { |sum, room| sum + room.value }
  end
end
