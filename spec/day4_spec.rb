require_relative '../exercises/day4'

RSpec.describe Day4 do
  it "figures out valid rooms checksum" do
    data = <<-DATA
    aaaaa-bbb-z-y-x-123[abxyz]
    a-b-c-d-e-f-g-h-987[abcde]
    not-a-real-room-404[oarel]
    totally-real-room-200[decoy]
    DATA
    day4 = Day4.new data
    result = day4.checksum
    expect(result).to eq 1514
  end

  it "figures out when the first intersection is" do
    data = 'qzmt-zixmtkozy-ivhz-343[z]'
    day4 = Day4.new data
    result = day4.rooms.first.decoded_name
    expect(result).to eq 'very encrypted name'
  end
end
