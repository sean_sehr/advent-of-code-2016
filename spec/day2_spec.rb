require_relative '../exercises/day2'

RSpec.describe Day2 do
  it "figures out the code on a square" do
    pad = Day2.new('square')
    code = <<-STR
      ULL
      RRDDD
      LURDL
      UUUUD
    STR
    result = pad.input(code, 5)
    expect(result).to eq "1985"
  end

  it "figures out the code on a diamond" do
    pad = Day2.new('diamond')
    code = <<-STR
      ULL
      RRDDD
      LURDL
      UUUUD
    STR
    result = pad.input(code, 5)
    expect(result).to eq "5DB3"
  end
end
