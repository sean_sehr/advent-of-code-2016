require_relative '../exercises/day5'

RSpec.describe Day5 do
  it "figures out the password" do
    data = 'abc'
    day5 = Day5.new(data, false)
    result = day5.result
    expect(result).to eq '18f47a30'
  end

  it "figures out the password when more encrypted" do
    data = 'abc'
    day5 = Day5.new(data, true)
    result = day5.result
    expect(result).to eq '05ace8e3'
  end
end
