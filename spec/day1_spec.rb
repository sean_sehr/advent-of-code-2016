require_relative '../exercises/day1'

RSpec.describe Day1 do
  it "figures out how far away we are" do
    data = 'R5, L5, R5, R3'
    day1 = Day1.new(data)
    result = day1.log.last.x.to_i + day1.log.last.y.to_i
    expect(result).to eq 12
  end

  it "figures out when the first intersection is" do
    data = 'R8, R4, R4, R8'
    day1 = Day1.new(data)
    result = day1.intersections.first.x.to_i + day1.intersections.first.y.to_i
    expect(result).to eq 4
  end
end
