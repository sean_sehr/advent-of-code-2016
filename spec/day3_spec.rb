require_relative '../exercises/day3'

RSpec.describe Day3 do
  it "figures out how many triangles are valid by row" do
    data = <<-STR
      5 10 25
      20 10 12
    STR
    day3 = Day3.new(data, 'row')
    result = day3.valid_triangles
    expect(result).to eq 1
  end

  it "figures out how many triangles are valid by columns" do
    data = <<-STR
    101 301 501
    102 302 502
    103 303 503
    201 401 601
    202 402 602
    203 403 1303
    STR
    day3 = Day3.new(data, 'column')
    result = day3.valid_triangles
    expect(result).to eq 5
  end
end
