# A class to hold cordinate data and easily get the sum of two cordinates.
class Point < Struct.new(:x, :y)

  # Get the sume of two cordinates
  def +(other)
    Point.new(self.x + other.x, self.y + other.y)
  end

  alias add +

  def ==(other)
    self.class === other and
      other.x == self.x and
      other.y == self.y
  end

  alias eql? ==
end
